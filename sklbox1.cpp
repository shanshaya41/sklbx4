﻿
#include <iostream>
#include <string>
using namespace std;

int main()
{
	int N;
	cout << "Enter the number N: ";
	cin >> N;
	cout << "Do you want even or odd numbers?" << endl;
	cout << "0: Even!" << endl;		//четные
	cout << "1: Odd!" << endl;		//нечетные
	int choice;
	do {
		cin >> choice;
	} while (choice != 1 && choice != 0);
	cout << "Ok! Here you are: ";
	switch (choice) {
	case 0: {
		for (int i = 0; i < N; i++)
			if (i % 2 == 0)
				cout << i << " ";
		break;
	}
	case 1: {
		for (int i = 0; i < N; i++)
			if (i % 2 == 1)
				cout << i << " ";
		break;
	}
	}
	cout << endl << endl;
	return 0;
}

